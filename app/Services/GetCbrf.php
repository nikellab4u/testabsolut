<?php
declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Http;
use App\ValValue;
use App\ListValutes;

class GetCbrf
{
    const HOSTNAME = 'http://www.cbr.ru/scripts/';


    public static function GetByDate($date): void
    {
        $dateCbFormat = str_replace('.', '/', $date);
        $dateBD = self::dateToBdFormat($date);

        $response = Http::get(self::HOSTNAME . 'XML_daily.asp', [
            'date_req' => $dateCbFormat,
        ]);
        if ($response->status() == 200) {
            $xml_object = simplexml_load_string($response->body());
            $xml_array = self::object2array($xml_object);
            //echo $xml_array["@attributes"]["Date"];
            if ($date == $xml_array["@attributes"]["Date"]) {
                //годный ответ, запишем
                foreach ($xml_array["Valute"] as $oneValute) {
                    $oneValute["Date"] = $date;
                    $listVal = ListValutes::where('char_code', $oneValute['CharCode'])->first();
                    if (!isset($listVal->id)) { //add to list
                        $valuta = new ListValutes;
                        $valuta->num_code = $oneValute['NumCode'];
                        $valuta->char_code = $oneValute['CharCode'];
                        $valuta->nominal = $oneValute['Nominal'];
                        $valuta->name = $oneValute['Name'];
                        $valuta->save();
                        $listVal = ListValutes::where('char_code', $oneValute['CharCode'])->first();
                    }
                    $curs = new ValValue;
                    $curs->amount = str_replace(',', '.', $oneValute['Value']);
                    $curs->date = $dateBD;
                    $curs->list_id = $listVal->id;
                    $curs->save();
                }
            }
        }
    }

    protected static function object2array($object)
    {
        return @json_decode(@json_encode($object), true);
    }

    public static function dateToBdFormat(string $date) :string
    {
        $dateBD = \DateTime::createFromFormat('d.m.Y', $date);
        $dateBD = date_format($dateBD, 'Y-m-d');
        return $dateBD;
    }

}
