<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ListValutes;

class ValValue extends Model
{
    protected $table = 'val_value';
    protected $fillable = ['id', 'amount', 'date', 'list_id'];

    function Curs() {
        return $this->belongsTo('App\ListValutes', 'list_id');
    }
}
