<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ValValue;
use App\SaveList;
use App\Services\GetCbrf;

class BankController extends Controller
{
    public function getByDate(Request $request, $date)
    {
        $dateBD = GetCbrf::dateToBdFormat($date);
        $currencies = ValValue::with('Curs')->where('date', $dateBD);
        if (!$currencies->count()) {
            //запрос серверу
            GetCbrf::GetByDate($date);
        }

        //обработка из бд
        $result = [];
        foreach ($currencies->get() as $currency) {
            $result[] = [
                'name' => $currency->Curs->name,
                'nominal' => $currency->Curs->nominal,
                'amount' => $currency->amount,
            ];
        }
        if (count($result)) {
            return response()->json($result);
        } else {
            return response()->json(['message' => 'params error'], 500);
        }

    }

    public function getByList(Request $request)
    {
        if ($request->has('date')) {
            $date = \DateTime::createFromFormat('d.m.Y', $request->input('date'));
        } else {
            $date = new \DateTime();
        }
        if ($request->has('main')) {
            $main = $request->input('main');
            if (!is_array($main)) {
                $mainArr[] = $main;
            } else {
                $mainArr = $main;
            }
        }
        if ($request->has('slave')) {
            $slave = $request->input('slave');
            if (!is_array($slave)) {
                $slaveArr[] = $slave;
            } else {
                $slaveArr = $slave;
            }
        }

        if ($date && isset($slave) && isset($main)) {
            $currencies = ValValue::with('Curs')->where('date', date_format($date, 'Y-m-d'));
            if (!$currencies->count()) {
                GetCbrf::GetByDate($date);
            }
            $result = [];
            $currencyList = $currencies->get();
            foreach ($mainArr as $oneMain) {
                $mainAmount = $this->getAmount($oneMain, $currencyList);
                if ($mainAmount == 0) continue;
                foreach ($slaveArr as $oneSlave) {
                    $slaveAmount = $this->getAmount($oneSlave, $currencyList);
                    if ($slaveAmount == 0) continue;
                    $result[$oneMain][$oneSlave] = $mainAmount/$slaveAmount;
                }
            }
            if ($request->has('save')){
                $serResult = serialize($result);
                if ($request->has('comment')){
                    $comment = $request->input('comment');
                }
                $list = new SaveList;
                $list->value = $serResult;
                $list->comment = $comment;
                $list->save();
            }
            return response()->json($result);
        } else {
            return response()->json(['message' => 'params error'], 500);
        }


    }

    public function getById(Request $request, $id)
    {
        $saveList = SaveList::findOrFail($id);
        return response()->json(unserialize($saveList->value));
    }

    protected function getAmount(string $currency, &$currencies): float
    {
        foreach ($currencies as $OneVal) {
            if ($currency == $OneVal->Curs->char_code) {
                return $OneVal->amount / $OneVal->Curs->nominal;
            }
        }
        return 0;
    }


    public function index()
    {
        return response()->json(['message' => 'params error'], 500);
    }
}
