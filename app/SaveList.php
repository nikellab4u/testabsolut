<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveList extends Model
{
    protected $table = 'save_list';
    protected $fillable = ['value', 'comment'];
}
