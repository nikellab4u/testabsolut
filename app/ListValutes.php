<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListValutes extends Model
{
    protected $table = 'val_list';
    protected $fillable = ['id', 'num_code', 'char_code', 'nominal', 'name'];

    public function user()
    {
        return $this->hasMany('App\ValValue', 'list_id');
    }
}
