<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('val_value', function (Blueprint $table) {
            $table->id();
            $table->decimal('amount', 10, 4);
            $table->date('date');
            $table->unsignedBigInteger('list_id');
            $table->foreign('list_id')->references('id')->on('val_list')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('val_value');
    }
}
