<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('api')->group(function () {
    Route::get('/get_by_date/{date}', 'BankController@getByDate')->
        where(['date' => '[0-3]+[0-9]+.+[0-1]+[0-9]+.+[2]+[0]+[0-2]+[0-9]']);
    Route::get('/get_by_list/', 'BankController@getByList');
    Route::get('/get_by_id/{id}', 'BankController@getById')->where('id', '^\d+$');
});
